import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Actions, IDispatchProps } from '../Actions/Actions';
import { IActionType, IStoreState } from '../Store/Store';

interface IStateProps {
  loginStatus: boolean;
  loading: boolean;
}

type TProps = IDispatchProps & IStateProps;

class App extends React.Component<TProps, {}> {
  public handleClick = () => {
    const { actions } = this.props;
    actions.onClick(0);
  }

  public handleLogin = () => {
    const { actions } = this.props;
    actions.onLogin();
  }

  public handleLogout = () => {
    const { actions } = this.props;
    actions.onLogout();
  }

  public render() {
    const { loginStatus, loading } = this.props;
    return (
      <div>
        <h3>
          Boilerplate
        </h3>
        {loading ? <p>Авторизация...</p> : loginStatus ? <p>Login success</p> : <p>Logged out</p>}
        <input
          className="btn btn-outline-secondary"
          disabled={loading}
          type="button"
          value="+"
          onClick={this.handleClick}
        />
        <input
          className="btn btn-outline-primary"
          disabled={loading}
          type="button"
          value="login"
          onClick={this.handleLogin}
        />
        <input
          className="btn btn-outline-warning"
          disabled={loading}
          type="button"
          value="logout"
          onClick={this.handleLogout}
        />
      </div>
    );
  }
}

function mapStateToProps(state: IStoreState): IStateProps {
  return {
    loginStatus: state.loginStatus,
    loading: state.loading,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
  return {
    actions: new Actions(dispatch),
  };
}

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export { connectApp as App };
