import { Dispatch } from 'redux';
import { IActionType } from '../Store/Store';
import { ActionTypes, AsyncActionTypes } from './Consts';

export interface IDispatchProps {
  actions: Actions;
}

export class Actions {
  constructor(private dispatch: Dispatch<IActionType>) {
  }

  public onClick = (i: number) => this.dispatch({ type: ActionTypes.CLICK, payload: i });

  public onLogin = () => {
    this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}` });

    // Простейший асинхронный экшен
    // setTimeout(() => {
    //   dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
    // }, 2000)

    // Экшен для запроса к РЕСТам
    // fetch('http://www.mocky.io/v2/5aafaf2c2d000048006eff2c') //404
    fetch('http://www.mocky.io/v2/5aafaf6f2d000057006eff31') // 200 - true
      // fetch('http://www.mocky.io/v2/5aafafa32d000056006eff3b') //200 - false
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        }
        throw new Error('error');
      })
      .then((data) => {
        // формат ответа:
        // {"data": {"authorized": true}}
        this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`, payload: data });
      })
      .catch((error) => {
        this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error });
      });
  }

  public onLogout = () => this.dispatch({ type: ActionTypes.LOGOUT });
}
