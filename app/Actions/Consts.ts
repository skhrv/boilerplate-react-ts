export enum ActionTypes {
  LOGIN = 'ACTION_LOGIN',
  LOGOUT = 'ACTION_LOGOUT',
  CLICK = 'ACTION_CLICK',
}

export enum AsyncActionTypes {
  BEGIN = '_BEGIN',
  SUCCESS = '_SUCCESS',
  FAILURE = '_FAILURE',
}
